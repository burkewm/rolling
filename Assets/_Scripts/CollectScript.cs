﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectScript : MonoBehaviour {

    public GameScript _gs;
    public AudioClip pickup;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other) {
        _gs.TotalCollected++;
        _gs.UpdateScore();
        AudioSource.PlayClipAtPoint(pickup, this.transform.position);
        Destroy(this.gameObject);
    }
}
