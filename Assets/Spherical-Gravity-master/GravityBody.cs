﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody))]
public class GravityBody : MonoBehaviour {
	
	GravityAttractor planet;
	Rigidbody rigidbody;
    public bool canAttract = false;
    void Awake () {
		planet = GameObject.FindGameObjectWithTag("Planet").GetComponent<GravityAttractor>();
		rigidbody = GetComponent<Rigidbody> ();

        // Disable rigidbody gravity and rotation as this is simulated in GravityAttractor scrip
        
            rigidbody.useGravity = true;
        
		//rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
	}

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Q)) {
            canAttract = true;
        }
        if (Input.GetKeyDown(KeyCode.R)) {
            canAttract = false;
        }
    }
    void FixedUpdate () {
        // Allow this body to be influenced by planet's gravity
        if (canAttract == true) {
            rigidbody.useGravity = false;
            planet.Attract(rigidbody);
        }
        if (canAttract == false) {
            rigidbody.useGravity = true;
        }



    }
}